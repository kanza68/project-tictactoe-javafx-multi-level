module org.example.tictactoe_javafx {
    requires javafx.controls;
    requires javafx.fxml;


    opens org.example.tictactoe_javafx to javafx.fxml;
    exports org.example.tictactoe_javafx;
}