package org.example.tictactoe_javafx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

 public class GameController implements Initializable {
    @FXML
    private Label winnerText;

    @FXML
    private Button btn_1;
    @FXML
    private Button btn_2;
    @FXML
    private Button btn_3;
    @FXML
    private Button btn_4;
    @FXML
    private Button btn_5;
    @FXML
    private Button btn_6;
    @FXML
    private Button btn_7;
    @FXML
    private Button btn_8;
    @FXML
    private Button btn_9;

    private int playerTurn = 0;

    ArrayList<Button> buttons;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        buttons = new ArrayList<>(Arrays.asList(btn_1, btn_2, btn_3, btn_4, btn_5, btn_6, btn_7, btn_8, btn_9));

        buttons.forEach(button ->{
            setupButton(button);
            button.setFocusTraversable(false);
        });
    }

    @FXML
    void restartGame(ActionEvent event){
        buttons.forEach(this::resetButton);
    }

    private void resetButton(Button button) {
        button.setDisable(false);
        button.setText("");
        winnerText.setText("TicTacToc | Morpion");
    }

    private void setupButton(Button button) {
        button.setOnMouseClicked(mouseEvent -> {
           setPlayerSymbol(button);
           button.setDisable(true);
           checkIfGameIsOver();
        });
    }



    private void setPlayerSymbol(Button button) {
        if(playerTurn % 2 == 0){
            button.setText("X");
            playerTurn = 1;
        } else {
            button.setText("0");
            playerTurn = 0;
        }
    }

    private void checkIfGameIsOver() {
        for (int a = 0; a < 8; a++){
            String line = switch (a) {
                case 0 -> btn_1.getText() + btn_2.getText() + btn_3.getText();
                case 1 -> btn_4.getText() + btn_5.getText() + btn_6.getText();
                case 2 -> btn_7.getText() + btn_8.getText() + btn_9.getText();
                case 3 -> btn_1.getText() + btn_5.getText() + btn_9.getText();
                case 4 -> btn_3.getText() + btn_5.getText() + btn_7.getText();
                case 5 -> btn_1.getText() + btn_4.getText() + btn_7.getText();
                case 6 -> btn_2.getText() + btn_5.getText() + btn_8.getText();
                case 7 -> btn_3.getText() + btn_6.getText() + btn_9.getText();
                default -> null;
            };

            // X winner
            if (line.equals("XXX")) {
                winnerText.setText("X won");
            }

            // O winner
            if (line.equals("OOO")) {
                winnerText.setText("O won");
            }
        }
    }
}