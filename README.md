# Project - Tictactoe JAVAFX Multi-Level 


> $\textcolor{red}{\text{Projet en cours de développement | Project currently under development}}$ 

 - [Version en Français](#item-one)
 - [English version](#item-two)

 <a id="item-one"></a>
 ## Version en Français

### Introduction
Ce projet est une idée que j'ai eu à la suite du développement de mon mini-projet de Morpion.

### Objectif de ce projet
Ce projet fait partie de mon objectif de maintien et d'amélioration de mes compétence pour cette année 2024.




<a id="item-two"></a>
## English version

### About the project
This project is an idea I had following the development of my TicTacToe mini-project.

### Goal of this project
This project is part of my objective to maintain and improve my skills for 2024.